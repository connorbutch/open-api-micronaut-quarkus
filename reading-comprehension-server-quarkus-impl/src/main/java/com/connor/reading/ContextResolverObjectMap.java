package com.connor.reading;

//import javax.ws.rs.ext.ContextResolver;
//import javax.ws.rs.ext.Provider;
//
//import com.fasterxml.jackson.annotation.JsonInclude;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * We wrote this class to avoid errors deserializing object in response.  However, now, when we deserialize either a pet or an error dto (which are generated),
 * they come back as empty json object {}.  This is the same issue I was having with micronaut (which caused me to switch and go back to quarkus).......
 * This makes me think it is related to open api codegen model class generation, something missing from config for models, or something unknown about graalvm.
 */
//@Provider
public class ContextResolverObjectMap /*implements ContextResolver<ObjectMapper>*/ {

//    private final ObjectMapper objectMapper = new ObjectMapper()
//            .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
//
//    @Override
//    public ObjectMapper getContext(Class<?> type) {
//        return objectMapper;
//    }
}