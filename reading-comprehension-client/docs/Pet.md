

# Pet

A pet object
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**name** | **String** |  | 
**tag** | **String** |  |  [optional]



