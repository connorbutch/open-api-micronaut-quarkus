package com.connor.reading.cdi;

import javax.inject.Named;
import javax.inject.Singleton;

@Named //implements old jsr-330, where beans must be explicitly named (bean discovery mode annotated)
public class DummyIntefaceToInjectFirstImpl implements  DummyIntefaceToInject {

    @Override
    public String getUniqueName() {
        return "blah!I'm unique somehow";
    }
}
