package com.connor.reading.cdi;

import javax.inject.Named;

//NOTE: unless have @Singleton is not detected as candidate for injection
@Named //try this to see if it gets picked up (note: no annotation will not get picked up)
public class DummyIntefaceToInjectSecondImpl implements DummyIntefaceToInject {
    @Override
    public String getUniqueName() {
        return "My mom tells me I'm special";
    }
}
