package com.connor.reading.util;

import com.connor.reading.exception.SystemException;
import io.micronaut.context.annotation.Requires;
import io.micronaut.core.exceptions.ExceptionHandler;
import io.micronaut.http.annotation.Produces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.ws.rs.core.Response;

@Produces
@Named //requires explicit registration of beans -- doesn't seem to be being picked up.......
//@Requires(classes = SystemException.class)
public class ExceptionHandlerImpl implements ExceptionHandler<Throwable/*, Response*/> { //TODO upgrade to higher version
    //of micronaut, so we can return a response rather than swallow the exception
    //these higher versions take in a second generic type parameter, which is the return type of the handle method

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerImpl.class);

    @Override
    public void handle(Throwable t) {
        LOGGER.error("\n\n\n\n\n\n\n\n\n\n********************Connor's custom exception handler", t);
        //TODO once uograde micronaut version, then can actually render (and more importantly, return) a jax rs response object
        //with the appropriate status code
    }
}
