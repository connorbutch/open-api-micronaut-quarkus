package com.connor.reading.service;

import com.connor.reading.cdi.DummyIntefaceToInject;
import io.micronaut.runtime.http.scope.RequestScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequestScope //for some reason, this is required, as the same instance can be injected twice..... might be updated in later versions of micronaut
public class HealthService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HealthService.class);

    private final Stream<DummyIntefaceToInject> dummyIntefaceToInjectStream;

     //this seems to work implicitly without the @Inject annotation present here
    public HealthService(Stream<DummyIntefaceToInject> dummyIntefaceToInjectStream){
        this.dummyIntefaceToInjectStream = dummyIntefaceToInjectStream;
    }


    public void debug(){
        LOGGER.info(dummyIntefaceToInjectStream.map(DummyIntefaceToInject::getUniqueName).collect(Collectors.joining(", ")));
    }

}
