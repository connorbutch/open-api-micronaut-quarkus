package com.connor.reading;

import io.micronaut.runtime.Micronaut;

import javax.inject.Singleton;

@Singleton
public class Application extends javax.ws.rs.core.Application{

    public static void main(String[] args) {
        Micronaut.run(Application.class, args);
    }
}
