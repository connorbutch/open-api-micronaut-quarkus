package com.connor.reading.configuration;

import com.connor.reading.dto.Error;
import com.connor.reading.dto.Pet;
import io.micronaut.core.annotation.Introspected;

/**
 * This exists to register the classes for introspection from another module.
 *
 * Even though they have the introspected annotation, they will always be serializes as {} without this class.
 */
@Introspected(classes = { Pet.class, Error.class})
public class IntrospectedConfiguration {
    //intentionally blank
}
