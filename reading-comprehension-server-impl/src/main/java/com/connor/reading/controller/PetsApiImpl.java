package com.connor.reading.controller;

import com.connor.reading.api.PetsApi;
import com.connor.reading.cdi.DummyIntefaceToInject;
import com.connor.reading.dto.Error;
import com.connor.reading.dto.Pet;
import com.connor.reading.service.HealthService;
import io.micronaut.http.annotation.Controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller //we have to add this to register this class with micronaut (since the api is in another jar.....)
public class PetsApiImpl implements PetsApi  {

    private final static Logger LOGGER = LoggerFactory.getLogger(PetsApiImpl.class);

    private final HealthService healthService;

    //NOTE: field injection does not work with graalvm
    //private final Stream<DummyIntefaceToInject> dummyIntefaceToInject;
    //NOTE: it seems to (possibly) reuse the same controller instance -- so when invoke this when injecting a stream, get this:
    /*
     Unexpected error occurred: stream has already been operated upon or closed
java.lang.IllegalStateException: stream has already been operated upon or closed
     */
    //while this doesn't support injecting javax.instance
    //like we are used to, it has better support for injecting native java constructs, such as a stream, iterator, etc
    //this could be used for (readiness) health check

    //this implicitly performs constructor injection without the need for the @Inject annotation
    public PetsApiImpl(HealthService healthService){
        this.healthService = healthService;
    }

    @Override
    public Response createPets() {
        LOGGER.info("Received a create pets request");
        Pet responseBody = new Pet();
        LOGGER.info("Response body {}", responseBody);
        return Response.ok(responseBody).build();
    }

    @Override
    public Response listPets(Integer limit) {
        LOGGER.info("Received a list pets request for limit {}", limit);

        //just to test cdi capabilities
        healthService.debug();

        //throw an exception at random to test our exception handler
        if(System.currentTimeMillis() %2 == 0) {
            throw new RuntimeException("Oh no, let's test our exception handler");
            //NOTE: This doesn't seem to trigger our exception handler ........
        }

        List<Pet> responseBody = Collections.singletonList(new Pet().name("Scruffy").tag("Chuck's pup").id(123L));
        LOGGER.info("Response body {}", responseBody);
        return Response.ok(responseBody).build();
    }

    @Override
    @Path("/{petId}") //for path params, we have to include this..... for some reason, it can't find this on interface declaration....
    public Response showPetById(String petId) {
        LOGGER.info("Received a request to get pet by id {}", petId);
        Error responseBody = new Error().code(1).message("Random error message");
        LOGGER.info("Response body {}", responseBody);
        return Response.status(Response.Status.NOT_FOUND).entity(responseBody).build();
    }
}
